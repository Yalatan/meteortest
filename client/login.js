// import { Template } from 'meteor/templating';
// import './login.html';

Template.login.events({
     "submit .login-form": function(event) {
         var email = $('[name=email]').val();
         var password = $('[name=password]').val();

         Meteor.loginWithPassword(email, password, function(err){
             if(err) {
                 console.log("Login failed");
                 return false;
             } else {
                 Router.go("/");
             }
         });
         return false;
     }
 });

Template.login.onRendered(function() {
    $("#login-link").addClass('selected');
    $("#home-link").removeClass('selected');
    $("#chart-link").removeClass('selected');
    $("#signup-link").removeClass('selected');

});