import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Users } from '../collections/users.js';


import './signup.html';



  Template.signup.events({
     "submit .register": function(event) {
       event.preventDefault();
       var email = $('[name=email]').val();
       var password = $('[name=password]').val();
        Accounts.createUser({
          email: email,
          password: password
        });

       event.target.email.value = '';
         event.target.password.value = '';
         Router.go("/");
       return false;
     }
  });

Template.signup.onRendered(function() {
    $("#signup-link").addClass('selected');
    $("#home-link").removeClass('selected');
    $("#login-link").removeClass('selected');
    $("#chart-link").removeClass('selected');

});


