import Highcharts from 'highcharts';
//import { Template } from 'meteor/templating';


Template.chart.onRendered(function () {

    Meteor.call('getChartData', function(err, series){
        let obj = {

            title: {
                text: 'Solar Employment Growth by Sector, 2010-2016'
            },

            subtitle: {
                text: 'Source: thesolarfoundation.com'
            },

            yAxis: {
                title: {
                    text: 'Number of Employees'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    pointStart: 2010
                }
            }

        };
        obj.series = series;

        $('#container').highcharts(obj);


    });
});

Template.chart.onRendered(function () {
    $("#chart-link").addClass('selected');
    $("#home-link").removeClass('selected');
    $("#login-link").removeClass('selected');
    $("#signup-link").removeClass('selected');

});

