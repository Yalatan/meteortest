Template.layout.events({
    "click .logout": function(event) {
        Meteor.logout(function(err) {
            Session.set("ses",false);
        });
        Router.go("/");
    }
});


Template.layout.helpers({
   email: function() {
       if (Meteor.user()) {
           return Meteor.user().emails[0].address;
       }
   }
});



