Template.articleForm.events({
    "submit .article-form": function() {
        var nameArticle = event.target.nameArticle.value;
        var contentArticle = event.target.contentArticle.value;

        Meteor.call('addArticles', nameArticle, contentArticle);

        event.target.nameArticle.value = '';
        event.target.contentArticle.value = '';
    }
});