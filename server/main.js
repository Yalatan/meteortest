import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'

import '../collections/users.js';

Meteor.startup(() => {
  // code to run on server at startup
});

Accounts.onLogin( (...a) =>
    console.log('account ',...a)
)